# Julia

> In Julia versions 1.5 and higher, by default the package manager connects to
> https://pkg.julialang.org, a free public service operated by the Julia project
> to serve open source package resources to Julia users.


**Note: this role is still lacking some capabilities!**
This role can handle updating Julia itself, but still lacks tasks that updates
installed Julia packages. Perhaps we could press `jlpkg` into service for that?



## Some links

+ [How to Install Julia on Ubuntu, Henrique Ferrolho's blog](https://ferrolho.github.io/blog/2019-01-26/how-to-install-julia-on-ubuntu)
+ [Julia Github repo](https://github.com/JuliaLang/julia)
+ [Julia downloads page](https://julialang.org/downloads/)
+ [Julia.jl — a manually curated taxonomy of Julia packages](https://github.com/svaksha/Julia.jl)


## What about Julia packages?

It seems Julia packages are installed for each user (in `~/.julia/environments/v1.5/`)
and that Julia comes with some sort of environments out of the box.

+ https://docs.julialang.org/en/v1/stdlib/Pkg
+ https://julialang.org/packages
+ https://github.com/JuliaRegistries/General
+ https://www.juliabloggers.com/delving-into-open-source-packages-for-julia
+ https://stackoverflow.com/questions/36398629/change-package-directory-in-julia
+ https://stackoverflow.com/questions/44220363/in-julia-how-do-you-check-list-update-your-packages
+ https://discourse.julialang.org/t/how-to-see-all-installed-packages-a-few-other-pkg-related-questions/1231/3
